import os

DBUSER = os.environ.get('CLOUDRON_POSTGRESQL_USERNAME', 'dbuser') 
DBPASS = os.environ.get('CLOUDRON_POSTGRESQL_PASSWORD', 'dbpass')
DBHOST = os.environ.get('CLOUDRON_POSTGRESQL_HOST', 'db')
DBNAME = os.environ.get('CLOUDRON_POSTGRESQL_DATABASE', 'dbname')
STATIC_DIR = "/app/code/static"
MEDIA_URL = "/media/"
STATIC_URL = "/static/"

OCR_DEFAULT_LANGUAGE = "eng"

OCR_LANGUAGES = {
    "eng": "English"
}
