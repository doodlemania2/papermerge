FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4
ARG DEBIAN_FRONTEND=noninteractive

RUN mkdir -p /app/code && mkdir /app/data
WORKDIR /app/code
RUN git clone https://github.com/ciur/papermerge.git /app/code

RUN add-apt-repository ppa:deadsnakes/ppa && apt install -y python3.8 python3.8-dev python3.8-venv

RUN apt-get update \
 && apt-get install -y \
                    build-essential \
                    vim \
                    virtualenv \
                    poppler-utils \
                    git \
                    imagemagick \
                    apache2 \
                    apache2-dev \
                    locales \
		    libapache2-mod-wsgi \
		    tesseract-ocr \
		    tesseract-ocr-eng \
		    default-jre-headless \
		    libcommons-lang3-java \
		    libbcprov-java \
 && rm -rf /var/lib/apt/lists/* \
 && pip3 install --upgrade pip

#USE STAPLER INSTEAD - leave for now though cause stapler isn't getting into the path for some reason from pip below
RUN wget http://launchpadlibrarian.net/383018194/pdftk-java_0.0.0+20180723.1-1_all.deb && \
    dpkg -i pdftk-java_0.0.0+20180723.1-1_all.deb

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE config.settings.production
#ENV PATH=/app/code:/app/code/.local/bin:$PATH
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen
ENV LANG=en_US.UTF-8 LC_ALL=en_US.UTF-8

RUN mkdir -p /app/data/media && mkdir -p /app/data/queue && mkdir -p /app/data/static && mkdir -p /app/data/htdocs && mkdir -p /app/data/python-eggs
COPY app.production.py /app/code/config/settings/production.py
COPY papermerge.config.py /app/code/papermerge-ready.conf.py
COPY startup.sh /app/code/startup.sh
RUN chmod +x /app/code/startup.sh
COPY create_user.py /app/code/create_user.py

ENV VIRTUAL_ENV=/app/code/.venv
RUN virtualenv $VIRTUAL_ENV -p /usr/bin/python3.8
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN git clone https://github.com/papermerge/mglib.git /app/code/mglib && \
	cd /app/code/mglib && \
	pip install setuptools && \
	pip install mglib && pip install stapler && \
	cd /app/code && \
	pip install -r requirements/base.txt --no-cache-dir && \
	pip install -r requirements/production.txt --no-cache-dir && \
	pip install -r requirements/extra/pg.txt --no-cache-dir
RUN ln -s /app/data/papermerge.conf.py /app/code/papermerge.conf.py && \
	ln -s /app/data/queue /app/code/queue && \
	ln -s /app/data/static /app/code/static && \
	#Yeah, this isn't smart - we need to move modwsgi somewhere / split it up so some can be in /app/data and some can go to /run... custom config maybe?

	#ln -s /app/data/httpd.conf /app/code/httpd.conf && \
	#ln -s /app/data/apachectl /app/code/apachectl && \
	#ln -s /app/data/default.wsgi /app/code/default.wsgi && \
	#ln -s /app/data/envvars /app/code/envvars && \
	#ln -s /run/error_log /app/code/error_log && \
	#ln -s /app/data/handler.wsgi /app/code/handler.wsgi && \
	#ln -s /app/data/htdocs /app/code/htdocs && \
	#ln -s /app/data/python-eggs /app/code/python-eggs && \
	#ln -s /app/data/httpd.conf /app/code/httpd.conf && \
	#ln -s /app/data/httpd.pid /app/code/httpd.conf && \
	#ln -s /app/data/resource.wsgi /app/code/resource.wsgi && \
	#ln -s /app/data/rewrite.conf /app/code/rewrite.conf && \
	
EXPOSE 8000
CMD ["/app/code/startup.sh"]
