#!/bin/bash

set -eu

echo "=> sending config"
if [ ! -f "/app/data/.configured" ]; then
	cp /app/code/papermerge-ready.conf.py /app/data/papermerge.conf.py
	./manage.py makemigrations
	./manage.py migrate
	cat create_user.py | python3 manage.py shell
	./manage.py collectstatic --no-input
	./manage.py check
	#./manage.py drop_triggers	
	touch /app/data/.configured
fi
echo "=> starting server"
mod_wsgi-express start-server \
	    --user cloudron --group cloudron \
	    --server-root /app/code \
	    --url-alias /static /app/code/static \ 
	    --url-alias /media /app/data/media \
	    --port 8000 \ 
	    --log-to-terminal \
	    --limit-request-body 20971520 \
	    config/wsgi.py &
#both of these will switch to execs
##### for whatever reason, stapler isn't showing in the PATH even though it's added with pip in docker - need to run that down
echo "=> starting worker"
python /app/code/manage.py worker &

